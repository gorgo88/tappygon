﻿

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UberController : MonoBehaviour {

	public float BP;

	const int MAX_ACTIVE_ZONES = 2;

	enum State {
		switching,
		final
	}

	struct Zone {
		public GameObject go;
		public bool isHitting;
		public bool isActive;
	}


	[SerializeField]
	GameObject _zoneParent;

	[SerializeField]
	GameObject _bgScene;

	Zone[] _zones;

	[SerializeField]
	bool _createMeshColliders;

	[SerializeField]
	Camera _zoneCamera;

	[SerializeField]
	float _touchSpeedThreshold = 0.1f;

	Color _zoneDefaultColor;

	[SerializeField]
	Color _zoneActiveColor;

	[SerializeField]
	Color _zoneHitColor;

	[SerializeField]
	Color _zoneHitActiveColor;

	[SerializeField]
	float _switchZoneTimeMax = 2.0f;

	[SerializeField]
	float _switchZoneTimeMin = 3.0f;

	[SerializeField]
	float _bpSpeedIncrease = 3.0f;

	[SerializeField]
	float _bpSpeedDecrease = 4.0f;

	[SerializeField]
	float _bpSpeedDecreaseFinal = 0.5f;

	[SerializeField]
	float[] _bpValues;

	[SerializeField]
	Text _bpMeterText;

	[SerializeField]
	AudioClip[] _musicClips;

	[SerializeField]
	AudioSource _audioSource;

	bool[] _currentFrameHitting;

	float _switchZoneTimeout;

	State _state;

	int _bpRange;

	void Start () {
		initZones();

		if (_createMeshColliders) 
			for (int z = 0; z < _zones.Length; ++z) 
				_zones[z].go.AddComponent<MeshCollider>();

		_zoneDefaultColor = _zones[0].go.GetComponent<MeshRenderer>().material.color;
		_currentFrameHitting = new bool[_zones.Length];

		reset();
	}

	void initZones ()
	{
		int zCount = _zoneParent.transform.childCount;
		_zones = new Zone[zCount];
		for (int z = 0; z < zCount; ++z) {
			_zones[z].go = _zoneParent.transform.GetChild(z).gameObject;
		}

	}

	void reset ()
	{
		_zoneParent.SetActive(true);
		for (int z = 0; z < _zones.Length; ++z) {
			_zones[z].isHitting = false;
			_zones[z].isActive = false;
		}
		BP = 0.0f;
		_switchZoneTimeout = getNextSwitchZoneTime();
		_state = State.switching;
		_bpRange = 0;
		switchAnimation(0);
	}

	float getNextSwitchZoneTime ()
	{
		return Random.Range(_switchZoneTimeMin, _switchZoneTimeMax);
	}
	
	void Update () {

		// reset hitting zones
		for (int z = 0; z < _zones.Length; ++z)
			_currentFrameHitting[z] = false;


		// check all zones for hitting in current frame
		List<Touch> touches = InputHelper.GetTouches();
		for (int i = 0; i < touches.Count; i++)
		{
			Touch touch = touches[i];

			float worldDeltaSpeed = _zoneCamera.ScreenToWorldPoint(new Vector3(
						touch.deltaPosition.x, 
			            touch.deltaPosition.y,
			            _zoneCamera.nearClipPlane)).sqrMagnitude / touch.deltaTime;

			if (Input.GetMouseButtonDown(0) || touch.phase == TouchPhase.Began || 
			    (touch.phase == TouchPhase.Moved && worldDeltaSpeed > _touchSpeedThreshold)) {

				Ray ray = Camera.main.ScreenPointToRay(touch.position);
				RaycastHit hit;
				Debug.DrawRay(ray.origin, ray.direction, Color.cyan);
				if (Physics.Raycast(ray, out hit, 100))
				{
					for (int z = 0; z < _zones.Length; ++z) {
						if (hit.collider.gameObject == _zones[z].go) {
							_currentFrameHitting[z] = true;
							break;
						}
					}
				}
			}
		}

		// update zones
		for (int z = 0; z < _zones.Length; ++z) {
			if (_zones[z].isHitting && !_currentFrameHitting[z])
				zoneHittingEnded(z);
			else if (!_zones[z].isHitting && _currentFrameHitting[z])
				zoneHittingStarted(z);
			_zones[z].isHitting = _currentFrameHitting[z];

			Color c = _zoneDefaultColor;

			if (_zones[z].isActive)
				if (_zones[z].isHitting)
					c = _zoneHitActiveColor;
				else 
					c = _zoneActiveColor;
			else if (_zones[z].isHitting)
				c = _zoneHitColor;

			_zones[z].go.GetComponent<Renderer>().material.color = c;
		}

		//switch zone timeout
		if (_state == State.switching) {
			_switchZoneTimeout -= Time.deltaTime;
			if (_switchZoneTimeout <= 0) {
				_switchZoneTimeout += getNextSwitchZoneTime();
				switchZones();
			}
		}

		//update BP
		if (_state == State.switching) {
			for (int z = 0; z < _zones.Length; ++z) {
				if (_zones[z].isActive) {
					if (_zones[z].isHitting) 
						BP += _bpSpeedIncrease * Time.deltaTime;
					else
						BP -= _bpSpeedDecrease * Time.deltaTime;

				}
			}

			//calculate current range state
			int r;
			for (r = 0; r < _bpValues.Length && BP >= _bpValues[r]; ++r) {}
			if (r == _bpValues.Length) {
				_state = State.final;
				_zoneParent.SetActive(false);
			}

			if (r != _bpRange) switchAnimation(r);
			_bpRange = r;
		}
		else if (_state == State.final) {
			BP -= _bpSpeedDecreaseFinal * Time.deltaTime;
			if (BP < 0.0f) 
				reset();
		}
		if (BP < 0.0f) BP = 0.0f;
		_bpMeterText.text = "BP: "+BP;

	}

	void switchZones ()
	{
		for (int z = 0; z < _zones.Length; ++z) {
			deactivateZone(z);
		}
		int count = Random.Range(1,MAX_ACTIVE_ZONES + 1);
		List<int> zIds = new List<int>();
		for (int z = 0; z < _zones.Length; ++z) zIds.Add(z);
		for (int i = 0; i < count; ++i) {
			int zIdId = Random.Range(0, zIds.Count); // ))
			int zId = zIds[zIdId];
			zIds.RemoveAt(zIdId);
			activateZone(zId);
		}
	}

	void deactivateZone (int index)
	{
		_zones[index].isActive = false;
	}

	void activateZone (int index)
	{
		_zones[index].isActive = true;
	}

	void zoneHittingEnded (int index)
	{
		Debug.Log("hit ended for" + index);
	}

	void zoneHittingStarted (int index)
	{
		Debug.Log("hit started for" + index);
	}

	void switchAnimation (int index)
	{
		_bgScene.GetComponent<Animation>().Play("anim_"+index);
		_audioSource.Stop();
		_audioSource.clip = _musicClips[index];
		_audioSource.Play();
	}

}
